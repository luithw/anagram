// main.cpp
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cctype>
#include <algorithm>
#include <map>
#include <ctime>
#include <cmath>
#include <set>
#include "main.h"

using namespace std;

// #define PRE_BUILD_TREE
// #define DEBUG
#define UNIGRAM_FILENAME "unigram.txt"
#define BIGRAM_FILENAME "bigram.txt"
#define LETTERS "abcdefghijklmnopqrstuvwxyz"
#define WORDS_FILENAME "project_words"
#define MAX_BIGRAM_DEPTH 1
#define MCTS_FORECAST_RATIO 0.45
#define MCTS_SAMPLES 5

class Distribution {

	public:
		map<string, float> unigram_prob;
		map<string, float> bigram_prob; 
		Distribution();
		void print();
		float get_entropy(string word);
		float get_unexplored_entropy(string word);

	private:
		string letters = LETTERS;
		map<char, unsigned> unigram_count;
		map<string, unsigned> bigram_count; 
		string line;					
		ifstream unigram_in;
		ifstream bigram_in;
		void initialise_bigram_higher(int depth, string str_minus);
		void compute_bigram_higher(int depth, string str_minus);
};

void Distribution::initialise_bigram_higher(int depth, string str_minus) {
    if (depth>=MAX_BIGRAM_DEPTH) {
    	return;
    }
    for (char& c_plus : letters) {
    	string new_str=str_minus+c_plus;    	
    	if (unigram_in.good() & bigram_in.good()) {
	    	getline(bigram_in, line);	    	
	    	// cout << str_minus << c_plus << ": " << stof(line) << endl;	    	
	    	bigram_prob[new_str]=stof(line);	       		
    	} else {
	    	bigram_prob[new_str]=0;	       		    		
    	}
    	initialise_bigram_higher(depth+1, new_str);
    }	
    return;  
}

void Distribution::compute_bigram_higher(int depth, string str_minus) {
    if (depth>=MAX_BIGRAM_DEPTH) {
    	return;
    }
    for (char& c_plus : letters) {
    	string new_str=str_minus+c_plus;   	    	
		bigram_prob[new_str]=bigram_prob[str_minus]+bigram_prob[string()+str_minus.back()+c_plus];	    	
    	compute_bigram_higher(depth+1, new_str);
    }	  
    return;  
}


Distribution::Distribution() : unigram_in(UNIGRAM_FILENAME), bigram_in(BIGRAM_FILENAME) {		

	if (unigram_in.good() & bigram_in.good()) {
		for(char& c_minus : letters) {
		  getline(unigram_in, line);
		  unigram_prob[string()+c_minus]=stof(line);
		  for(char& c : letters) {
		    getline(bigram_in, line);
		    // cout << c_minus << c << ": " << stof(line) << endl;
		    string new_str=string()+c_minus+c;
		    bigram_prob[new_str]=stof(line);  
		    initialise_bigram_higher(1, new_str);
		  }
		}
	} else {
		// Initialise all the unigram_count and bigram_count pairs to 0
		for(char& c_minus : letters) {
		  unigram_count[c_minus]=0;
		  for(char& c : letters) {
		    string new_str=string()+c_minus+c;		  	
		    bigram_count[new_str]=0;
		    initialise_bigram_higher(1, new_str);
		  }
		}

		ifstream infile(WORDS_FILENAME);
		unsigned int total_count=0;
		while (getline(infile, line))
		{
		  transform(line.begin(), line.end(), line.begin(), ::tolower);    //Transform all strings to lower case
		  char c_minus='@';
		  bool all_alphabets=true;
		  for(char& c : line) {
		  	if (!isalpha(c)) {
		  		all_alphabets=false;
		  		break;
		  	}
		  }

		  if (!all_alphabets) {
		  	continue;
		  }

		  for(char& c : line) {
		      if (isalpha(c)) {
		        unigram_count[c]++;
		        total_count++;
		      }
		      if (c_minus!='@') {
		        if (isalpha(c_minus) & isalpha(c)) {
		          bigram_count[string()+c_minus+c]++;
		        }
		      }
		      c_minus=c;
		  }
		}

		// Compute the probability distribution
		for(char& c_minus : letters) {
		  unigram_prob[string()+c_minus]=-log2((float)unigram_count[c_minus]/(float)total_count);
		  unsigned int c_minus_count = 0;
		  for(char& c : letters) {
		    c_minus_count+=bigram_count[string()+c_minus+c];
		  }     
		  for(char& c : letters) {
		    bigram_prob[string()+c_minus+c]=-log2((float)bigram_count[string()+c_minus+c]/(float)c_minus_count);        
		  }          
		}

		// Compute the bigram square
		for(char& c_minus : letters) {
		  for(char& c : letters) {
		    string new_str=string()+c_minus+c;		  	
		    compute_bigram_higher(1, new_str);
		  }
		}


		//Save the unigram_count distribution
		ofstream unigram_out(UNIGRAM_FILENAME);
		for(auto e : unigram_prob)
		{
		    unigram_out << e.second << endl;      
		}  
		unigram_out.close();

		//Save the bigram_count distribution
		ofstream bigram_out(BIGRAM_FILENAME);      
		for(auto e : bigram_prob)
		{
		    bigram_out << e.second << endl;      
		}  
		bigram_out.close();
	}
}

void Distribution::print() {

	//Print the unigram_count distribution
	for(auto e : unigram_prob)
	{
	  cout << e.first << ":" << e.second << endl;
	}  

	//Print the bigram_count distribution
	for(auto e : bigram_prob)
	{
	  cout << e.first << ":" << e.second << endl;
	}  

	cout << "Bigram length: " << bigram_prob.size() << endl;

	return;
}

float Distribution::get_unexplored_entropy(string word) {
	float entropy=0;
	for(unsigned int i=1; i<word.length(); i++) {
	  	entropy+=bigram_prob[string()+word[i-1]+word[i]];	  		  	
	}
	return entropy;
}

float Distribution::get_entropy(string word) {
	float entropy=0;
	char c_minus='@';
	for(char& c : word) {
	  if (c_minus=='@') {
	  	entropy+=unigram_prob[string()+c];	  		  	
	  } else {
	  	entropy+=bigram_prob[string()+c_minus+c];	  		  	
	  }
	  c_minus=c;
	}
	return entropy;
}

class Tree {

	public:
		map<string, set<string>> edges;	//This is a map of parent children pair of the tree
		Tree(string target);
		void print();
		set<string> expand_node(string parent);	
		string get_unexplored(string parent);				

	private:
		string target;
		void permutate(set<string>& permutations, string unexplored, string head, int depth);
};

Tree::Tree(string t) {
	target=t;
	set<string> first_childern;
	for (char& c : t) {
		first_childern.insert(string()+c);
	}
	edges[""]=first_childern;
	#ifdef PRE_BUILD_TREE
		for (auto node : first_childern) {
			auto next_children = expand_node(""+node);		
		}
	#endif
}

void Tree::permutate(set<string>& permutations, string unexplored, string head, int depth) {
	if (depth==0 || unexplored.length()==0){
		permutations.insert(head);
		return;
	}
	for (unsigned int i=0; i<unexplored.length(); i++) {
		string remain = unexplored;
		remain.erase(i,1);
		permutate(permutations, remain, head+unexplored[i], depth-1);
	}
	return;
}

string Tree::get_unexplored(string parent) {
	// Need to work out which characters are not used yet
	string parent_copy = parent;	
	string unexplored="";
	for (char& c : target) {
		auto found = parent_copy.find(c);
		if (found!=string::npos) {
			parent_copy.erase(found,1);
		} else {
			unexplored+=c;
		}
	}
	return unexplored;
}

set<string> Tree::expand_node(string parent) {
	
	set<string> children;
	string unexplored = get_unexplored(parent);	
	permutate(children, unexplored, "", MAX_BIGRAM_DEPTH);
	edges[parent] = children;

	#ifdef PRE_BUILD_TREE
		for (auto node : children) {
			string next_node=parent+node;
			if (next_node.length()<target.length()) {
				auto next_children = expand_node(next_node);
			}		
					
		}	
	#endif

	return children;
}

void Tree::print(){

	for (auto edge:edges) {
  		for (auto child: edge.second) {
			cout << "Parent: " << edge.first << ", child: " << child << endl;
  		}		
	}

	return;
}

class Dijkstra_MCTS {
	public:
		set<pair<float, string>> top_anagrams;		
		Dijkstra_MCTS(string target, Distribution& distribution, Tree& tree);
	private:
		set<pair<float, pair<float, string>>> priority_queue;		
		string u = "";
		float min_distance=0;
		float node_entropy=0;
		char c_minus;		
		float simulation(string v, Distribution& distribution, Tree& tree);	
};

float Dijkstra_MCTS :: simulation(string v, Distribution& distribution, Tree& tree) {
	float entropy_sum=0;	
	int count=0;
	string unexplored = tree.get_unexplored(v);	
	for (unsigned int i=0; i<MCTS_SAMPLES; i++) {
		random_shuffle(unexplored.begin(), unexplored.end());
		float entropy = distribution.get_unexplored_entropy(string()+v.back()+unexplored);
		if (!isinf(entropy)) {
			entropy_sum+=entropy;
			count++;
		}
	  	#ifdef DEBUG
			cout << "Parent: " << v << ", unexplored sample: " << unexplored << ", entropy_sum: " << entropy_sum << endl ;
		#endif						
	}
	float entropy_mean;
	if (count>0) {
		entropy_mean=entropy_sum/count;
	} else {
		entropy_mean=INFINITY;
	}
	return entropy_mean;
}

Dijkstra_MCTS :: Dijkstra_MCTS(string target, Distribution& distribution, Tree& tree){

	//Initalise priority queue
	for (auto child : tree.edges[u]) {
		string v = u + child;
		float past_entropy=distribution.unigram_prob[string()+child];	
		float entropy_forecast=simulation(v, distribution, tree);	
		float total_entropy=past_entropy+entropy_forecast*MCTS_FORECAST_RATIO;	
	  	#ifdef DEBUG
			cout << v << ", total_entropy: " << total_entropy << " past_entropy: " << past_entropy << ", entropy_forecast: " << entropy_forecast << endl ;		
		#endif					
		priority_queue.insert({total_entropy, {past_entropy, v}});			
	}

	while (!priority_queue.empty()) {

		min_distance = priority_queue.begin()->first;
		node_entropy = priority_queue.begin()->second.first;
		u = priority_queue.begin()->second.second;
		c_minus=u.back();
		priority_queue.erase({min_distance, {node_entropy, u}});
	  	#ifdef DEBUG
			cout << "\n\nStart of loop, u: " << u << ", min_distance:" << min_distance << endl;
		#endif	

		if (u.length()==target.length()) {
		  	#ifdef DEBUG
				cout << "Found top anagram: " << u << endl << endl;
			#endif				
			top_anagrams.insert({node_entropy, u});

			if (top_anagrams.size()>=5) {
				break;
			} else {
				continue;
			}	

		}

	#ifdef PRE_BUILD_TREE
		for (auto child : tree.edges[u]) {
	#else
		for (auto child : tree.expand_node(u)) {
	#endif			
			string v = u + child;
			float edge_entropy = distribution.bigram_prob[string()+c_minus+child];
			float past_entropy=node_entropy+edge_entropy;
			float entropy_forecast=simulation(v, distribution, tree);
			float total_entropy=past_entropy+entropy_forecast*MCTS_FORECAST_RATIO;					
			priority_queue.insert({total_entropy, {past_entropy, v}});			
		  	#ifdef DEBUG
				cout << "Adding v: " << v << ", total_entropy: " << total_entropy << " past_entropy: "<< past_entropy << ", entropy_forecast: " << entropy_forecast << endl;
			#endif								
		}
	}

	return;
}

int main(int argc, char** argv)
{
	int start_s=clock();

	Distribution distribution;
	// distribution.print();

	string target = argv[1];
  	#ifdef DEBUG
		float entropy = distribution.get_entropy(target);
		cout << "Target word: " << target << ", entropy: " << entropy << endl;
	#endif		

	Tree tree(target);
  	#ifdef DEBUG
		tree.print();
	#endif		

	Dijkstra_MCTS solver(target, distribution, tree);

	for (auto e : solver.top_anagrams) {
		cout << e.second << " " <<e.first << endl;
	}

	int stop_s=clock();
	cout << "(time spent: " << (stop_s-start_s)/double(CLOCKS_PER_SEC) << " seconds)" << endl;  

	return 0;
}