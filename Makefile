CXX=g++
CXXFLAGS=-g -std=c++11 -Wall -pedantic -static-libstdc++ -static -o3
BIN=top5anagram

SRC=$(wildcard *.cpp)
OBJ=$(SRC:%.cpp=%.o)

all: $(OBJ)
	$(CXX) -o $(BIN) $^

%.o: %.c
	$(CXX) $@ -c $<

clean:
	rm -f *.o
	rm $(BIN)

run: 
	./top5anagram	